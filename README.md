- **Pages**. Your website should have at least three pages:  
    - one for Google Search,  **(done)**  
    - one for Google Image Search, and  **(done)**  
    - one for Google Advanced Search.  **(done)**  
On the Google Search page, there should be links in the upper-right of the page to go to Image Search or Advanced Search.  **(done)**  
On each of the other two pages, there should be a link in the upper-right to go back to Google Search. **(done)**  
- **Query Text**. On the Google Search page, the user should be able to type in a query, click “Google Search”, 
    and be taken to the Google search results for that page.  
    Like Google’s own, your search bar should be  
        - centered with rounded corners.  **(done)**  
        - The search button should also be centered, and should be beneath the search bar.  **(done)**  
- **Query Images**.  
    On the Google Image Search page,  
        - the user should be able to type in a query,  **(done)**  
        - click a search button, and  **(done)**  
        - be taken to the Google Image search results for that page.  **(done)**  
- **Query Advanced**.  
    On the Google Advanced Search page,  
        the user should be able to provide input for the following four fields (taken from Google’s own advanced search options)  
        - Find pages with… “all these words:”  **(done)**  
        - Find pages with… “this exact word or phrase:”  **(done)**  
        - Find pages with… “any of these words:”  **(done)**  
        - Find pages with… “none of these words:”  **(done)**  
- **Appearance**.  
    Like Google’s own Advanced Search page, the four options should be stacked vertically, and all of the text fields should be left aligned.  **(done)**  
    Consistent with Google’s own CSS,  
        - the “Advanced Search” button should be blue with white text.  **(done)**  
        - When the “Advanced Search” button is clicked, the user should be taken to search results page for their given query.  **(done)**  
- **Lucky**.  
    Add an “I’m Feeling Lucky” button to the main Google Search page.  
    Consistent with Google’s own behavior, clicking this link should take users directly to the first Google search result for the query, bypassing the normal results page.  
- **Aesthetics**.  
    The CSS you write should match Google’s own aesthetics as best as possible.
